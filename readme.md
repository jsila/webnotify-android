# Web Notify Android

App that sends notification to web server. You can filter which apps are allowed to send notifications.

User has to specify username and password otherwise server won't save notifications for him/her.

There's also "sending to email inbox" feature in progress.

See screenshots folder for app preview (both Android and web).

Also on my account you can find web app which accompany this android app.