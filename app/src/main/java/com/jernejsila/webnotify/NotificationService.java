package com.jernejsila.webnotify;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class NotificationService extends NotificationListenerService {

    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Notification notification = sbn.getNotification();

        String pkg = sbn.getPackageName();
        Bundle extras = notification.extras;
        String title = extras.getString("android.title");
        CharSequence text = extras.getCharSequence("android.text");
        String textString = text != null ? text.toString() : "";

        PackageManager packageManager= getApplicationContext().getPackageManager();
        String appName = pkg;
        try {
            appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(pkg, PackageManager.GET_META_DATA));
        } catch (PackageManager.NameNotFoundException ignored) {}

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String retention = preferences.getString("retention", "24");
        Set<String> enabledApps = preferences.getStringSet(EnabledAppsActivity.ENABLED_APPS_PREF_KEY, new HashSet<String>());

        if ((notification.flags & Notification.FLAG_FOREGROUND_SERVICE) != 0
                || (notification.flags & Notification.FLAG_ONGOING_EVENT) != 0
                || (notification.flags & Notification.FLAG_LOCAL_ONLY) != 0) {
            //This is not a notification we want!
            return;
        }

        if (!enabledApps.contains(pkg)) {
            return;
        }

        JSONObject serverParams = new JSONObject();
        try {
            serverParams.put("text", textString);
            serverParams.put("title", title);
            serverParams.put("package", pkg);
            serverParams.put("retention", Integer.parseInt(retention));
            serverParams.put("app", appName);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Log.i("time", sdf.format(new Date()));
            serverParams.put("timestamp", sdf.format(new Date()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest serverRequest = new JsonObjectRequest(Request.Method.POST, getString(R.string.server_url), serverParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("server request", "success");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("server request", "failed");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String username = preferences.getString("username", "").trim();
                String password = preferences.getString("password", "").trim();
                String data = username + ":" + password;
                String authorization = "Basic " + Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);

                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", authorization);
                return headers;
            }
        };

        RequestQueue requestQueue = Requests.getInstance(context).getRequestQueue();
        requestQueue.add(serverRequest);

        String email = preferences.getString("email", "");
        boolean sendEmail = preferences.getBoolean("send_email", false);
        if (!sendEmail || email.equals("")) {
            return;
        }

        JSONObject mailgunParams = new JSONObject();
        try {
            mailgunParams.put("from", "JS Notifier <noreply@jernejsila.com>");
            mailgunParams.put("to", email);
            mailgunParams.put("subject", "New notification from your phone");
            mailgunParams.put("text", String.format("from:%s\ntitle: %s\ntext: %s", appName, title, textString));
            mailgunParams.put("html", String.format("<h1>From</h1><p>%s</p><h1>Title</h1><p>%s</p><h1>Text</h1><p>%s</p>", appName, title, textString));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest mailgunRequest = new JsonObjectRequest(Request.Method.POST, getString(R.string.mailgun_url), mailgunParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("mailgun request", "success");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("mailgun request", "failed");

                        String body = "";
                        if (error.networkResponse.data!=null) {
                            try {
                                body = new String(error.networkResponse.data,"UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("error body", body);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String apiSecret = getString(R.string.mailgun_api_key);
                byte[] data = ("api:" + apiSecret).getBytes();
                String authorization = "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);

                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", authorization);
                return headers;
            }
        };

        requestQueue.add(mailgunRequest);
    }
}
