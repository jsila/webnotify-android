package com.jernejsila.webnotify;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.SectionIndexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;

class AppListAdapter extends BaseAdapter implements SectionIndexer {

    private Context context;
    private AppListInfo[] apps;
    private LinkedHashMap<String, Integer> mapIndex;
    private String[] sections;

    public AppListAdapter(Context context, AppListInfo[] apps) {
        this.context = context;
        this.apps = apps;

        mapIndex = new LinkedHashMap<>();

        for (int i = 0; i < apps.length; i++) {
            String ch = apps[i].name.substring(0, 1).toUpperCase(Locale.US);
            mapIndex.put(ch, i);
        }

        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<>(mapIndex.keySet());

        Log.d("sectionList", sectionList.toString());
        Collections.sort(sectionList);

        sections = new String[sectionList.size()];

        sectionList.toArray(sections);

    }

    @Override
    public int getCount() {
        return apps.length;
    }

    @Override
    public AppListInfo getItem(int position) {
        return apps[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_multiple_choice, null, true);
        }
        CheckedTextView checkedTextView = (CheckedTextView)view;
        checkedTextView.setText(apps[position].name);
        checkedTextView.setCompoundDrawablesWithIntrinsicBounds(apps[position].icon, null, null, null);
        checkedTextView.setCompoundDrawablePadding((int)(8*context.getResources().getDisplayMetrics().density));

        return view;
    }

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int i) {
        return mapIndex.get(sections[i]);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }
}
