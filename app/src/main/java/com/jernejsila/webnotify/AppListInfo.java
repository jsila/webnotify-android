package com.jernejsila.webnotify;

import android.graphics.drawable.Drawable;

class AppListInfo {
    String pkg;
    String name;
    Drawable icon;
    boolean isEnabled;
}
