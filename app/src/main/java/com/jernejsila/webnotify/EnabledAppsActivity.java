package com.jernejsila.webnotify;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EnabledAppsActivity extends AppCompatActivity {
    public static final String ENABLED_APPS_PREF_KEY = "com.jernejsila.webnotify.preferences.ENABLED_APPS";

    AppListInfo[] apps;
    Set<String> enabledApps;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enabled_apps);
        getSupportActionBar().setTitle(R.string.enabled_apps_name);

        context = this;
        enabledApps = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getStringSet(ENABLED_APPS_PREF_KEY, new HashSet<String>());

        new Thread(new Runnable() {
            @Override
            public void run() {
                PackageManager packageManager = getPackageManager();
                List<ApplicationInfo> appList = packageManager.getInstalledApplications(0);
                int count = appList.size();

                apps = new AppListInfo[count];
                for (int i = 0; i < count; i++) {
                    ApplicationInfo appInfo = appList.get(i);
                    apps[i] = new AppListInfo();
                    apps[i].pkg = appInfo.packageName;
                    apps[i].name = appInfo.loadLabel(packageManager).toString();
                    apps[i].icon = resizeIcon(appInfo.loadIcon(packageManager), 48);
                    apps[i].isEnabled = enabledApps.contains(appInfo.packageName);
                }

                Arrays.sort(apps, new Comparator<AppListInfo>() {
                    @Override
                    public int compare(AppListInfo lhs, AppListInfo rhs) {
                        return StringsHelper.compare(lhs.name, rhs.name);
                    }
                });

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayAppList();
                    }
                });
            }
        }).start();
    }

    void displayAppList() {
        final ListView listView = findViewById(R.id.lvFilterApps);
        AppListAdapter adapter = new AppListAdapter(getApplicationContext(), apps);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                boolean checked = listView.isItemChecked(i);
                String pkg = apps[i].pkg;

                if (enabledApps.contains(pkg)) {
                    enabledApps.remove(pkg);
                } else {
                    enabledApps.add(pkg);
                }

                PreferenceManager.getDefaultSharedPreferences(context)
                        .edit()
                        .putStringSet(ENABLED_APPS_PREF_KEY, enabledApps)
                        .apply();

                apps[i].isEnabled = checked;
            }
        });

        for (int i = 0 ; i < apps.length; i++) {
            listView.setItemChecked(i, apps[i].isEnabled);
        }

        listView.setVisibility(View.VISIBLE);
        findViewById(R.id.spinner).setVisibility(View.GONE);
    }

    Drawable resizeIcon(Drawable icon, int maxSize) {
        Resources res = getResources();

        maxSize = (int)(maxSize*res.getDisplayMetrics().density);

        Bitmap bitmap = Bitmap.createBitmap(maxSize, maxSize, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        icon.draw(canvas);

        return new BitmapDrawable(res, bitmap);
    }
}

